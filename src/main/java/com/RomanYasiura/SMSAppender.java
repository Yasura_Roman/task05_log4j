package com.RomanYasiura;

import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

import java.io.Serializable;

@Plugin(name = "SMS", category = "Core", elementType = "appender", printObject = true)
public final class SMSAppender extends AbstractAppender {

    protected SMSAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
    }

    @Override
    public void append(LogEvent logEvent) {
//        try {
//            SMSSender.send(new String(getLayout().toByteArray(logEvent)));
//        } catch (Exception ex){}
        SMSSender.send(logEvent.getMessage().getFormattedMessage());
    }

    @PluginFactory
    public static SMSAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute
    ){
        if (name == null){
            LOGGER.error("No name provided");
            return null;
        }
        if (layout == null){
            layout = PatternLayout.createDefaultLayout();
        }
        return new SMSAppender(name,filter,layout,true);
    }
}
