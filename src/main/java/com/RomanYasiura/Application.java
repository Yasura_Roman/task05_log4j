package com.RomanYasiura;
import org.apache.logging.log4j.*;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class.getName());

    public static void main(String[] args) {
        logger.trace("trace");
        logger.debug("debug");
        logger.info("info");
        logger.warn("warm");
        logger.error("error");
        logger.fatal("fatal");
    }
}
